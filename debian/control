Source: libparallel-iterator-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: gregor herrmann <gregoa@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 13)
Build-Depends-Indep: perl
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libparallel-iterator-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libparallel-iterator-perl.git
Homepage: https://metacpan.org/release/Parallel-Iterator
Rules-Requires-Root: no

Package: libparallel-iterator-perl
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends}
Description: module for simple parallel execution
 Parallel::Iterator provides a 'parallel map()'. Multiple worker processes are
 forked so that many instances of the transformation function may be executed
 simultaneously.
 .
 For time consuming operations, particularly operations that spend most of
 their time waiting for I/O, this is a big performance win. It also provides a
 simple idiom to make effective use of multi CPU systems.
